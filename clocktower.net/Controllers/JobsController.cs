﻿using clocktower.net.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace clocktower.net.Controllers
{
    public class JobsController : Controller
    {
        // GET: Jobs
        [HttpGet]
        public ActionResult Index()
        {
            ViewData.Model = JobScheduler.GetAllJobs();
            return View();
        }

        [HttpPost]
        public ActionResult Index(PostJob newJob)
        {
            JobScheduler.AddPostJob(newJob);
            return this.RedirectToAction("Index");
        }

        [HttpDelete]
        public ActionResult Index(string jobId)
        {
            JobScheduler.DeleteJob(jobId);
            return new EmptyResult();
        }
    }
}