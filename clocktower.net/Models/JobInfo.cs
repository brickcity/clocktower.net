﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace clocktower.net.Models
{
    public class JobInfo
    {
        public string Name { get; set; }
        public DateTimeOffset? NextRun { get; set; }
        public DateTimeOffset? PrevRun { get; set; }
    }
}