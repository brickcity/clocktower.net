﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace clocktower.net.Models
{
    public class PostJob
    {
        public Uri Url { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public bool Repeat { get; set; }
        public int? EveryUnit { get; set; }
        public string Interval { get; set; }
    }
}