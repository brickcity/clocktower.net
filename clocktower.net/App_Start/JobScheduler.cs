﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace clocktower.net
{
    using System.Collections.Specialized;

    using Quartz;
    using Quartz.Impl;
    using System.Net;

    using Quartz.Impl.Matchers;
    using clocktower.net.Models;

    public class JobScheduler
    {
        public static void Start()
        {
            var scheduler = GetScheduler();
            scheduler.Start();

            var keepAliveJob = JobBuilder.Create<KeepMeAlive>()
                .WithIdentity("KeepAlive", "ClockTower")
                .Build();

            var trigger =
                TriggerBuilder.Create()
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInMinutes(5)
                        .RepeatForever())
                    .Build();

            try
            {
                scheduler.ScheduleJob(keepAliveJob, trigger);
            }
            catch (ObjectAlreadyExistsException)
            {
                
            }
        }

        public static void AddPostJob(PostJob job)
        {
            var scheduler = GetScheduler();
            var details =
                JobBuilder.Create<PostToUrlJob>()
                    .WithIdentity(job.Url.ToString(), "Posts")
                    .UsingJobData("url", job.Url.ToString())
                    .Build();

            var trigger = TriggerBuilder.Create().StartAt(job.StartDate);
            var ts = Interval(job.Interval, job.EveryUnit);

            if (job.Repeat)
            {
                trigger.WithSimpleSchedule(x => x.WithInterval(ts).RepeatForever());
            }

            scheduler.ScheduleJob(details, trigger.Build());
        }

        public static void DeleteJob(string jobId)
        {
            var scheduler = GetScheduler();
            scheduler.DeleteJob(new JobKey(jobId, "Posts"));
        }

        private static TimeSpan Interval(string interval, double? unit)
        {
            if (!unit.HasValue) unit = 1;

            switch (interval)
            {
                case "seconds":
                    return TimeSpan.FromSeconds(unit.Value);
                case "hours":
                    return TimeSpan.FromHours(unit.Value);
                case "days":
                    return TimeSpan.FromDays(unit.Value);
                default:
                    return TimeSpan.FromMinutes(unit.Value);
            }
        }

        public static IEnumerable<JobInfo> GetAllJobs()
        {
            var scheduler = GetScheduler();
            var jobGroups = scheduler.GetJobGroupNames();
            var results = new List<JobInfo>();

            foreach (string group in jobGroups)
            {
                var groupMatcher = GroupMatcher<JobKey>.GroupContains(group);
                var jobKeys = scheduler.GetJobKeys(groupMatcher);
                foreach (var jobKey in jobKeys)
                {
                    var triggers = scheduler.GetTriggersOfJob(jobKey);
                    foreach (var trigger in triggers)
                    {
                        results.Add(new JobInfo()
                                        {
                                            Name = jobKey.Name,
                                            NextRun = trigger.GetNextFireTimeUtc(),
                                            PrevRun = trigger.GetPreviousFireTimeUtc()
                                        });
                    }
                }
            }

            return results;
        }

        public static void Stop()
        {
            var scheduler = GetScheduler();
            scheduler.Shutdown(true);
        }

        private static IScheduler GetScheduler()
        {
            // get a scheduler
            var properties = new NameValueCollection();
            properties["quartz.scheduler.instanceName"] = "ClockTower"; // needed if you plan to use the same database for many schedulers
            properties["quartz.scheduler.instanceId"] = Environment.MachineName + DateTime.UtcNow.Ticks; // requires uniqueness
            properties["quartz.jobStore.type"] = "Quartz.Impl.MongoDB.JobStore, Quartz.Impl.MongoDB";

            return new StdSchedulerFactory(properties).GetScheduler();            
        }
    }

    [PersistJobDataAfterExecution]
    public class KeepMeAlive : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var pingRequest = (HttpWebRequest)WebRequest.Create("http://localhost:47773");
            var response = pingRequest.GetResponse();
            context.Result = response;
        }
    }

    [PersistJobDataAfterExecution]
    public class PostToUrlJob : IJob
    {

        public void Execute(IJobExecutionContext context)
        {
            var data = context.JobDetail.JobDataMap;
            var postRequest = (HttpWebRequest)WebRequest.Create(data.GetString("url"));
            postRequest.Method = "POST";
            postRequest.ContentLength = 0;
            postRequest.GetResponseAsync().ContinueWith((r) => context.Result = r.Result);
        }
    }
}